<?php
namespace App\Service;

class ArrayService
{
    /**
     * Znajduje liczby, które się nie powtarzają
     *
     * @param $input array Tablica liczb
     * @return array
     */
    public function findSingle(array $input): array
    {
        $input = array_map('strval', $input);
        $counts = array_count_values($input);
        $filtered = array_filter($counts, function($value) {
            return $value === 1;
        });

        return array_keys($filtered);
    }
}
