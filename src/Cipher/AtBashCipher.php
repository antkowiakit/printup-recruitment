<?php
namespace App\Cipher;

use App\Cipher\Traits\CharConverter;
use App\Cipher\Traits\ValidateChar;

final class AtBashCipher implements CiphersContract
{
    use CharConverter, ValidateChar;

    private $a_z;
    private $z_a;

    public function __construct()
    {
        $this->a_z = range('a', 'z');
        $this->z_a = range('z', 'a');
    }

    private function encode(string $input): string
    {
        $chars = str_split($input);
        foreach ($chars as &$char) {
            if (!$this->isCharValid($this->convertCharToNumber($char))) {
                throw new \InvalidArgumentException('Invalid input');
            };
            $char = $this->z_a[array_search($char, $this->a_z)];
        }

        return implode('', $chars);
    }

    public function encrypt(string $input): string
    {
        return $this->encode($input);
    }

    public function decrypt(string $input): string
    {
        return $this->encode($input);
    }
}
