<?php
namespace App\Cipher;

use App\Cipher\Traits\CharConverter;
use App\Cipher\Traits\ValidateChar;

final class CaesarCipher implements CiphersContract
{
    use CharConverter, ValidateChar;

    const OFFSET = 3;

    const ENCRYPT = 0;
    const DECRYPT = 1;

    private function encode(string $input, string $type): string
    {
        $chars = str_split($input);
        foreach ($chars as &$char) {
            $char = $this->convertCharToNumber($char);
            if (!$this->isCharValid($char)) {
                throw new \InvalidArgumentException('Invalid input');
            };
            $char = $this->{$this->resolveFunction($type)}($char);
            $char = $this->convertNumberToChar($char);
        }

        return implode('', $chars);
    }

    private function resolveFunction(string $type): string
    {
        switch ($type) {
            case self::ENCRYPT:
                return 'addOffset';
            break;
            case self::DECRYPT:
                return 'subOffset';
            break;
        }

        throw new Exception('Invalid type');
    }

    public function encrypt(string $input): string
    {
        return $this->encode($input, self::ENCRYPT);
    }

    public function decrypt(string $input): string
    {
        return $this->encode($input, self::DECRYPT);
    }

    private function addOffset(int $number): int
    {
        return $this->fixOverflow($number + self::OFFSET);
    }

    private function subOffset(int $number): int
    {
        return $this->fixOverflow($number - self::OFFSET);
    }

    private function fixOverflow(int $number): int
    {
        if ($number > self::Z_CODE) {
            return $number - self::NUMBER_OF_CHARS;
        }
        if ($number < self::A_CODE) {
            return $number + self::NUMBER_OF_CHARS;
        }

        return $number;
    }
}
