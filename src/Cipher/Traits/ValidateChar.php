<?php
namespace App\Cipher\Traits;

use App\Cipher\CiphersContract;

trait ValidateChar
{
    private function isCharValid(int $charNumber): bool
    {
        return ($charNumber >= CiphersContract::A_CODE) && ($charNumber <= CiphersContract::Z_CODE);
    }
}
