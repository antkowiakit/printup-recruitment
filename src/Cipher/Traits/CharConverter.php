<?php
namespace App\Cipher\Traits;

trait CharConverter
{
    private function convertCharToNumber(string $char): int
    {
        return ord($char);
    }

    private function convertNumberToChar(int $number): string
    {
        return chr($number);
    }
}
