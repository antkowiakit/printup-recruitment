<?php
namespace App\Cipher;

use App\Cipher\Traits\CharConverter;
use App\Cipher\Traits\ValidateChar;

final class BaconCipher implements CiphersContract
{
    use CharConverter, ValidateChar;

    private function charNumberToBaconCode(int $number): string //@todo: private it!
    {
        return str_replace([0, 1], ['a', 'b'], str_pad(decbin($number), 5, 0, STR_PAD_LEFT));
    }

    private function baconCodeToCharNumber(string $code): string //@todo: private it!
    {
        return bindec(str_replace(['a', 'b'], [0, 1], $code));
    }

    public function encrypt(string $input): string
    {
        $chars = str_split($input);
        foreach ($chars as &$char) {
            $char = $this->convertCharToNumber($char);
            if (!$this->isCharValid($char)) {
                throw new \InvalidArgumentException('Invalid input');
            };
            $char = $this->charNumberToBaconCode($char);
        }

        return implode(' ', $chars);
    }

    public function decrypt(string $input): string
    {
        $chars = explode(' ', $input);
        foreach ($chars as &$char) {
            $char = $this->baconCodeToCharNumber($char);
            if (!$this->isCharValid($char)) {
                throw new \InvalidArgumentException('Invalid input');
            };
            $char = $this->convertNumberToChar($char);
        }

        return implode('', $chars);
    }
}
