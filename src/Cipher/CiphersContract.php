<?php

namespace App\Cipher;

interface CiphersContract
{
    const A_CODE = 97;
    const Z_CODE = 122;
    const NUMBER_OF_CHARS = 26;

    public function encrypt(string $input): string;

    public function decrypt(string $input): string;
}
