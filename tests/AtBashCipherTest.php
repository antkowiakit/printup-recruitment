<?php
namespace Tests;

use PHPUnit\Framework\TestCase;
use App\Cipher\AtBashCipher;

class AtBashCipherTest extends TestCase
{
    public function testEncrypt()
    {
        $atBashCipher = new AtBashCipher();
        $result = $atBashCipher->encrypt('abc');

        $this->assertEquals('zyx', $result);
    }

    public function testEncryptWithInvalidInput()
    {
        $this->expectException(\InvalidArgumentException::class);

        $atBashCipher = new AtBashCipher();
        $atBashCipher->encrypt('abc~');
    }

    public function testEncryptZ()
    {
        $atBashCipher = new AtBashCipher();
        $result = $atBashCipher->encrypt('z');

        $this->assertEquals('a', $result);
    }

    public function testDecryptC()
    {
        $atBashCipher = new AtBashCipher();
        $result = $atBashCipher->decrypt('c');

        $this->assertEquals('x', $result);
    }

    public function testDecryptWithInvalidInput()
    {
        $this->expectException(\InvalidArgumentException::class);

        $atBashCipher = new AtBashCipher();
        $atBashCipher->decrypt('abc~');
    }
}
