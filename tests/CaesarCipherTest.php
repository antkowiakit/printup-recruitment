<?php
namespace Tests;

use PHPUnit\Framework\TestCase;
use App\Cipher\CaesarCipher;

class CaesarCipherTest extends TestCase
{
    public function testEncrypt()
    {
        $caesarCipher = new CaesarCipher();
        $result = $caesarCipher->encrypt('abc');

        $this->assertEquals('def', $result);
    }

    public function testEncryptWithInvalidInput()
    {
        $this->expectException(\InvalidArgumentException::class);

        $caesarCipher = new CaesarCipher();
        $caesarCipher->encrypt('abc~');
    }

    public function testEncryptZ()
    {
        $caesarCipher = new CaesarCipher();
        $result = $caesarCipher->encrypt('z');

        $this->assertEquals('c', $result);
    }

    public function testDecryptC()
    {
        $caesarCipher = new CaesarCipher();
        $result = $caesarCipher->decrypt('c');

        $this->assertEquals('z', $result);
    }

    public function testDecryptWithInvalidInput()
    {
        $this->expectException(\InvalidArgumentException::class);

        $caesarCipher = new CaesarCipher();
        $caesarCipher->decrypt('abc~');
    }
}
