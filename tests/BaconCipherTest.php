<?php
namespace Tests;

use PHPUnit\Framework\TestCase;
use App\Cipher\BaconCipher;

class BaconCipherTest extends TestCase
{
    public function testEncrypt()
    {
        $baconCipher = new BaconCipher();
        $result = $baconCipher->encrypt('abc');

        $this->assertEquals('bbaaaab bbaaaba bbaaabb', $result);
    }

    public function testEncryptWithInvalidInput()
    {
        $this->expectException(\InvalidArgumentException::class);

        $baconCipher = new BaconCipher();
        $baconCipher->encrypt('abc~');
    }

    public function testDecrypt()
    {
        $baconCipher = new BaconCipher();
        $result = $baconCipher->decrypt('bbbabaa bbaabab bbbaabb bbbabaa');

        $this->assertEquals('test', $result);
    }

    public function testDecryptWithInvalidInput()
    {
        $this->expectException(\InvalidArgumentException::class);

        $baconCipher = new BaconCipher();
        $baconCipher->decrypt('abc~');
    }
}
