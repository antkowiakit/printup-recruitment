<?php
namespace Tests;

use PHPUnit\Framework\TestCase;
use App\Service\ArrayService;

class ArrayServiceTest extends TestCase
{
    public function testFindSingleOneResult()
    {
        $arrayService = new ArrayService();
        $result = $arrayService->findSingle([1, 2, 3, 4, 1, 2, 3]);

        $this->assertEquals([4], $result);
    }

    public function testFindSingleMultipleResult()
    {
        $arrayService = new ArrayService();
        $result = $arrayService->findSingle([11, 21, 33.4, 18, 21, 33.39999, 33.4]);

        $this->assertEquals([11, 18, 33.39999], $result);
    }

    public function testFindSingleNoResult()
    {
        $arrayService = new ArrayService();
        $result = $arrayService->findSingle([11, 11, 22, 22, 22]);

        $this->assertEquals([], $result);
    }
}
