## How to run?
1. Clone repo
2. Build `docker-compose build`
3. Run `docker-compose up -d`
4. Enter into container `docker-compose exec php-fpm bash`
5. Install node dependencies `composer install`

## How to run tests?
`vendor/phpunit/phpunit/phpunit`

## How to generate coverage?
`vendor/phpunit/phpunit/phpunit --coverage-html coverage_dir`
